package src.shop.clients;

import src.shop.interfaces.IGood;

public class VipVisitor extends BaseVisitor {

    private Integer discount;


    public VipVisitor(String name, Integer discount) {
        super(name);
        this.discount = discount;
    }

    public boolean checkDiscount() {
        return discount > 0;
    }

    @Override
    public void buy(IGood good) {
        if (checkDiscount()){

        }else {
            super.buy(good);
        }
    }
}
