package src.shop.start;

import src.shop.SalesRoom;
import src.shop.clients.Visitor;
import src.shop.departments.ElectronicDepartment;
import src.shop.departments.GameDepartment;
import src.shop.goods.Computer;
import src.shop.goods.GameConsole;
import src.shop.goods.HardDrive;
import src.shop.goods.Televisor;
import src.shop.service.*;
import src.shop.enums.ConsultResult;

public class Main {

    public static String SHOP_NAME;

    public static void main(String[] args) {

        imitateShopWorking();

    }

    public static void imitateShopWorking(){

        SalesRoom salesRoom=new SalesRoom(SHOP_NAME);
        Administrator administrator=new Administrator("AdministratorName");
        ElectronicDepartment electronicDepartment=new ElectronicDepartment();
        GameDepartment gameDepartment=new GameDepartment("GameDepartmentName");

        salesRoom.addDepartment(electronicDepartment);
        salesRoom.addDepartment(gameDepartment);

        Banker bankerElecDep=new Banker(true);
        Cashier cashierElecDep=new Cashier(true);
        Consultant consultantElecDep=new Consultant(true);
        Security securityElecDep=new Security(true);

        Banker bankerGame=new Banker(true);
        Cashier cashierGame=new Cashier(true);
        Consultant consultantGame=new Consultant(true);
        Security securityGame=new Security(true);

        electronicDepartment.addEmployee(bankerElecDep);
        electronicDepartment.addEmployee(cashierElecDep);
        electronicDepartment.addEmployee(consultantElecDep);
        electronicDepartment.addEmployee(securityElecDep);

        gameDepartment.addEmployee(bankerGame);
        gameDepartment.addEmployee(cashierGame);
        gameDepartment.addEmployee(consultantGame);
        gameDepartment.addEmployee(securityGame);

        Computer computer=new Computer("comp");
        GameConsole gameConsole=new GameConsole("console");
        HardDrive hardDrive=new HardDrive("harddrive");
        Televisor televisor=new Televisor("televisor");

        electronicDepartment.addGood(computer);
        gameDepartment.addGood(gameConsole);
        electronicDepartment.addGood(hardDrive);
        electronicDepartment.addGood(televisor);

        Visitor visitor1=new Visitor("FirstVisitor");
        Visitor visitor2=new Visitor("SecondVisitor");

        Consultant forFirstVisitor=administrator.getFreeConsultant(electronicDepartment);
        Consultant forSecondVisitor=administrator.getFreeConsultant(gameDepartment);

        ConsultResult consultResultForFirst=forFirstVisitor.consult(visitor1);
        ConsultResult consultResultForSecond=forSecondVisitor.consult(visitor2);

        switch (consultResultForFirst){
            case BUY:
                visitor1.buy(televisor);
                break;
            case EXIT:
                break;
        }

        switch (consultResultForSecond){
            case BUY:
                visitor2.buy(gameConsole);
                break;
            case EXIT:
                break;
        }

    }

}
