package src.shop;

import src.shop.clients.BaseVisitor;
import src.shop.departments.BaseDepartment;
import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IVisitor;

import java.util.ArrayList;

public class SalesRoom {

    private String name;
    ArrayList<IVisitor> BaseVisitor=new ArrayList<>();
    ArrayList<IDepartment> BaseDepartment=new ArrayList<>();


    public SalesRoom(String name, ArrayList<IVisitor> baseVisitor, ArrayList<IDepartment> baseDepartment) {
        this.name = name;
        BaseVisitor = baseVisitor;
        BaseDepartment = baseDepartment;
    }

    public SalesRoom(String name) {
        this.name = name;
    }

    public void addDepartment(BaseDepartment department) {
        BaseDepartment.add(department);
    }

    public void addVisitor(BaseVisitor visitor) {
        BaseVisitor.add(visitor);
    }

}
