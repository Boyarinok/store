package src.shop.service;

import src.shop.enums.ConsultResult;
import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IVisitor;

public class Consultant extends BaseEmployees {

    public Consultant(IDepartment department, String name, boolean free) {
        super(department, name, free);
    }

    public ConsultResult consult(IVisitor iVisitor){
        super.setFree(false);

        return ConsultResult.BUY;
    }

    public void send() {

    }

    public Consultant(boolean free) {
        super(free);
    }

}
