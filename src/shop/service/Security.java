package src.shop.service;

import src.shop.interfaces.IDepartment;

public class Security extends BaseEmployees {

    public Security(IDepartment department, String name, boolean free) {
        super(department, name, free);
    }

    public void openDoor() {

    }

    public void closeDoor() {

    }

    public void checkVisitor() {

    }

    public Security(boolean free) {
        super(free);
    }

}
