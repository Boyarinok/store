package src.shop.service;

import src.shop.SalesRoom;
import src.shop.interfaces.IDepartment;
import src.shop.interfaces.IEmployee;

import java.util.ArrayList;

public class Administrator {

    private String name;
    ArrayList<SalesRoom> salesRoom;
    private boolean free;

    public Administrator(String name, ArrayList<SalesRoom> salesRoom, boolean free) {
        this.name = name;
        this.salesRoom = salesRoom;
        this.free = free;
    }

    public Administrator(String name) {
        this.name = name;
    }

    public Consultant getFreeConsultant(IDepartment iDepartment){
        for (IEmployee employee:iDepartment.getEmployeeList()){
            if (employee instanceof Consultant){
                if (employee.isFree()){
                    return (Consultant)employee;
                }
            }
        }
        return null;
    }

    public boolean isFree() {
        return free;
    }

}
