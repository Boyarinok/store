package src.shop.interfaces;

public interface IEmployee {

    boolean isFree();
    String getName();
    IDepartment getDepartment();

}
