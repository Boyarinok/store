package src.shop.interfaces;

public interface IVisitor {

    void buy(IGood good);
    void returnGoods();
    String getName();

}
