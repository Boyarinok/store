package src.shop.goods;

import src.shop.departments.BaseDepartment;

public class GameConsole extends ElectronicDevice {

    private String ram;

    public GameConsole(BaseDepartment department, String name, boolean hasGuarantee, String price, String company, String ram) {
        super(department, name, hasGuarantee, price, company);
        this.ram = ram;
    }

    public void on() {

    }

    public void loadGame() {

    }

    public GameConsole(String name) {
        super(name);
    }
}
